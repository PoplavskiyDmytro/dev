public class Helper {
    public static boolean cycleHelper(int randomNumber) {
        boolean flag = false;
        while (!flag) {
            int inputNumber = Input.inputNumber();
            flag = GuessNumber.compareNumber(inputNumber, randomNumber);
            System.out.println(Helper.check(inputNumber, randomNumber));
        }
        return true;
    }

    public static String check(int inputNumber, int generateNumber) {
        int result;
        result = generateNumber - inputNumber;
        if (result < 0) {
            result = result * (-1);
        }
        if (result == 0) {
            return "Ви вгадали";
        }
        if (result <= 10) {
            return "Гаряче";
        } else if (result <= 25) {
            return "Тепло";
        } else {
            return "Холодно";
        }

    }
}
