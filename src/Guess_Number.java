import java.util.Scanner;

class GuessNumber {

    public static void main(String[] args) {

        cycleHelper(generateNumber());
    }

    public static boolean compareNumber(int userNumber, int randomNumber) {
        if (userNumber == randomNumber) {
            return true;
        }
        System.out.println("Ви не вгадали число");
        return false;
    }

    public static int inputNumber() {
        System.out.println("Введіть число");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    public static int generateNumber() {
        int generetaNumber = (int) (1 + Math.random() * 100);
        System.out.println(generetaNumber);
        return generetaNumber;
    }

    public static boolean cycleHelper(int randomNumber) {
        boolean flag = false;
        while (!flag) {
            int inputNumber = inputNumber();
            flag = compareNumber(inputNumber, randomNumber);
            System.out.println(check(inputNumber, randomNumber));
        }
        return true;
    }

    public static String check(int inputNumber, int generateNumber) {
        int result;
        result = generateNumber - inputNumber;
        if (result < 0) {
            result = result * (-1);
        }
        if (result <= 10) {
            return "Гаряче";
        } else if (result <= 25) {
            return "Тепло";
        } else {
            return "Холодно";
        }
    }
}
